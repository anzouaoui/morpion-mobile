package com.zouaoui.fr.morpion;

import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by zouaoui on 02/01/2017.
 */

public class MorpionOrdinateurActivity extends AppCompatActivity {
    Button buttonA1, buttonA2, buttonA3, buttonB1, buttonB2, buttonB3, buttonC1, buttonC2, buttonC3;
    Button[] buttons = new Button[9];
    MediaPlayer music;
    TextView gameStatus;
    Button buttonReplay;
    ImageButton buttonPlay;
    ImageButton buttonStop;
    ImageView imageViewMorpion;
    Toolbar toolbar;
    boolean gameIsOver = false;
    boolean againstComputer = false;
    int tour = 0;
    String[] symbol = {"X", "O"};
    private View.OnClickListener replayListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            for (Button currentButton : buttons) {
                currentButton.setText("");
            }
            gameStatus.setText(R.string.gameStatus);
            tour = 0;
            gameIsOver = false;
        }
    };
    private View.OnClickListener playListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            music = MediaPlayer.create(MorpionOrdinateurActivity.this, R.raw.bach);
            music.start();
        }
    };
    private View.OnClickListener stopListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            music.stop();
        }
    };
    private View.OnClickListener buttonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //Trouver le bouton sur lequel on clique
            Button buttonSelected = (Button) findViewById(v.getId());

            //Verifier si le jeu est terminé
            if (gameIsOver) {
                return;
            }

            //Verifier si l'emplacement est valide
            if (!isValid(buttonSelected)) {
                //Mettre un animation

                //Afficher que le placement est invalide
                gameStatus.setText("Placement invalide");
            } else {
                setSymbol(buttonSelected, symbol[tour]);

                //Verfifier si il y a un gagnant
                gameIsOver = winnerExists();
                if (gameIsOver) {
                    String winner = "";
                    if (tour == 0) {
                        gameStatus.setText("Le joueur " + winner + " a gaggné");
                    } else {
                        gameStatus.setText("L'ordinateur à gagné");
                    }
                }

                //Si le tableau est plein
                if (boardIsFull()) {
                    gameStatus.setText("Match nul !!");
                    gameIsOver = true;
                    return;
                }
                if (tour == 0) {
                    tour = 1;
                } else {
                    tour = 0;
                }

                //Tour de l'ordnateur
                if (tour == 1 && againstComputer) {
                    buttonSelected = mouvementOrdinateur();
                    buttonSelected.setText(symbol[0]);
                }
            }
        }
    };

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.morpion_ordinateur_main);

            initialize();
            BindEventListener();
        }

    private void initialize() {
        buttonA1 = (Button) findViewById(R.id.buttonA1);
        buttonA2 = (Button) findViewById(R.id.buttonA2);
        buttonA3 = (Button) findViewById(R.id.buttonA3);
        buttonB1 = (Button) findViewById(R.id.buttonB1);
        buttonB2 = (Button) findViewById(R.id.buttonB2);
        buttonB3 = (Button) findViewById(R.id.buttonB3);
        buttonC1 = (Button) findViewById(R.id.buttonC1);
        buttonC2 = (Button) findViewById(R.id.buttonC2);
        buttonC3 = (Button) findViewById(R.id.buttonC3);
        buttonPlay = (ImageButton) findViewById(R.id.imageButtonPlay);
        buttonStop = (ImageButton) findViewById(R.id.imageButtonStop);
        gameStatus = (TextView) findViewById(R.id.gameStatusTextView);
        buttonReplay = (Button) findViewById(R.id.buttonReplay);
        imageViewMorpion = (ImageView) findViewById(R.id.morpion);
        music = MediaPlayer.create(MorpionOrdinateurActivity.this, R.raw.bach);
        music.start();

        buttons[0] = buttonA1;
        buttons[1] = buttonA2;
        buttons[2] = buttonA3;
        buttons[3] = buttonB1;
        buttons[4] = buttonB2;
        buttons[5] = buttonB3;
        buttons[6] = buttonC1;
        buttons[7] = buttonC2;
        buttons[8] = buttonC3;

    }

    private void BindEventListener() {
        for (Button button : buttons) {
            button.setOnClickListener(buttonListener);
        }
        buttonReplay.setOnClickListener(replayListener);

        buttonPlay.setOnClickListener(playListener);
        buttonStop.setOnClickListener(stopListener);
    }

    protected boolean isValid(Button button) {
        return button.getText().toString().length() == 0;
    }

    protected boolean winnerExists() {
        boolean gagner = false;
        /**
         * HORIZONTAL
         */
        if ((buttons[0].getText().toString() == symbol[tour]) && (buttons[1].getText().toString() == symbol[tour]) && (buttons[2].getText().toString() == symbol[tour])) {
            gagner = true;
        }
        if ((buttons[3].getText().toString() == symbol[tour]) && (buttons[4].getText().toString() == symbol[tour]) && (buttons[5].getText().toString() == symbol[tour])) {
            gagner = true;
        }
        if ((buttons[6].getText().toString() == symbol[tour]) && (buttons[7].getText().toString() == symbol[tour]) && (buttons[8].getText().toString() == symbol[tour])) {
            gagner = true;
        }

        /**
         * VERTICAL
         */
        if ((buttons[0].getText().toString() == symbol[tour]) && (buttons[3].getText().toString() == symbol[tour]) && (buttons[6].getText().toString() == symbol[tour])) {
            gagner = true;
        }
        if ((buttons[1].getText().toString() == symbol[tour]) && (buttons[4].getText().toString() == symbol[tour]) && (buttons[7].getText().toString() == symbol[tour])) {
            gagner = true;
        }
        if ((buttons[2].getText().toString() == symbol[tour]) && (buttons[5].getText().toString() == symbol[tour]) && (buttons[8].getText().toString() == symbol[tour])) {
            gagner = true;
        }

        /**
         * DIAGONAL
         */
        if ((buttons[0].getText().toString() == symbol[tour]) && (buttons[4].getText().toString() == symbol[tour]) && (buttons[8].getText().toString() == symbol[tour])) {
            gagner = true;
        }
        if ((buttons[2].getText().toString() == symbol[tour]) && (buttons[4].getText().toString() == symbol[tour]) && (buttons[6].getText().toString() == symbol[tour])) {
            gagner = true;
        }
        return gagner;
    }

    protected boolean boardIsFull() {
        for (Button currentButton : buttons) {
            if (isValid(currentButton)) {
                return false;
            }
        }
        return true;
    }

    protected Button mouvementOrdinateur() {
        Button mouvement = null;

        mouvement = WinOrBlock("O");
        if (mouvement == null) {
            mouvement = WinOrBlock("X");
            if (mouvement == null) {
                mouvement = Coin();
                if (mouvement == null) {
                    mouvement = EspaceVide();
                }
            }
        }
        return mouvement;
    }

    protected void setSymbol(Button button, String symbol) {
        button.setText(symbol);
        if (button.getText().toString() == "X") {
            button.setTextColor(Color.rgb(0, 166, 166));
        } else if (button.getText().toString() == "O") {
            button.setTextColor(Color.rgb(138, 87, 17));
        }
    }

    private Button WinOrBlock(String marque) {
        Button currentButton = null;
        /**
         * HORIZONTAL
         */
        if (buttonA1.getText().toString() == marque && buttonA2.getText().toString() == marque && buttonA3.getText().toString() == "") {
            currentButton = buttonA3;
        }
        if (buttonA2.getText().toString() == marque && buttonA3.getText().toString() == marque && buttonA1.getText().toString() == "") {
            currentButton = buttonA1;
        }
        if (buttonA3.getText().toString() == marque && buttonA1.getText().toString() == marque && buttonA2.getText().toString() == "") {
            currentButton = buttonA2;
        }

        if (buttonB1.getText().toString() == marque && buttonB2.getText().toString() == marque && buttonB3.getText().toString() == "") {
            currentButton = buttonB3;
        }
        if (buttonB2.getText().toString() == marque && buttonB3.getText().toString() == marque && buttonB1.getText().toString() == "") {
            currentButton = buttonB1;
        }
        if (buttonB3.getText().toString() == marque && buttonB1.getText().toString() == marque && buttonB2.getText().toString() == "") {
            currentButton = buttonB2;
        }

        if (buttonC1.getText().toString() == marque && buttonC2.getText().toString() == marque && buttonC3.getText().toString() == "") {
            currentButton = buttonC3;
        }
        if (buttonC2.getText().toString() == marque && buttonC3.getText().toString() == marque && buttonC1.getText().toString() == "") {
            currentButton = buttonC1;
        }
        if (buttonC3.getText().toString() == marque && buttonC1.getText().toString() == marque && buttonC2.getText().toString() == "") {
            currentButton = buttonC2;
        }

        /**
         * VERTICAL
         */
        if (buttonA1.getText().toString() == marque && buttonB1.getText().toString() == marque && buttonC1.getText().toString() == "") {
            currentButton = buttonC1;
        }
        if (buttonB1.getText().toString() == marque && buttonC1.getText().toString() == marque && buttonA1.getText().toString() == "") {
            currentButton = buttonA1;
        }
        if (buttonC1.getText().toString() == marque && buttonA1.getText().toString() == marque && buttonB1.getText().toString() == "") {
            currentButton = buttonB1;
        }

        if (buttonA2.getText().toString() == marque && buttonB2.getText().toString() == marque && buttonC2.getText().toString() == "") {
            currentButton = buttonC2;
        }
        if (buttonB2.getText().toString() == marque && buttonC2.getText().toString() == marque && buttonA2.getText().toString() == "") {
            currentButton = buttonA2;
        }
        if (buttonC2.getText().toString() == marque && buttonA2.getText().toString() == marque && buttonB2.getText().toString() == "") {
            currentButton = buttonB2;
        }

        if (buttonA3.getText().toString() == marque && buttonB3.getText().toString() == marque && buttonC3.getText().toString() == "") {
            currentButton = buttonC3;
        }
        if (buttonB3.getText().toString() == marque && buttonC3.getText().toString() == marque && buttonA3.getText().toString() == "") {
            currentButton = buttonA3;
        }
        if (buttonC3.getText().toString() == marque && buttonA3.getText().toString() == marque && buttonB3.getText().toString() == "") {
            currentButton = buttonB3;
        }

        /**
         * DIAGONAL
         */
        if (buttonA1.getText().toString() == marque && buttonB2.getText().toString() == marque && buttonC3.getText().toString() == "") {
            currentButton = buttonC3;
        }
        if (buttonB2.getText().toString() == marque && buttonC3.getText().toString() == marque && buttonA1.getText().toString() == "") {
            currentButton = buttonA1;
        }
        if (buttonC3.getText().toString() == marque && buttonA1.getText().toString() == marque && buttonB2.getText().toString() == "") {
            currentButton = buttonB2;
        }

        if (buttonA3.getText().toString() == marque && buttonB2.getText().toString() == marque && buttonC1.getText().toString() == "") {
            currentButton = buttonC1;
        }
        if (buttonB2.getText().toString() == marque && buttonC1.getText().toString() == marque && buttonA3.getText().toString() == "") {
            currentButton = buttonA3;
        }
        if (buttonC1.getText().toString() == marque && buttonA3.getText().toString() == marque && buttonB2.getText().toString() == "") {
            currentButton = buttonB2;
        }
        return currentButton;
    }

    private Button Coin() {
        Button currentButton = null;
        if (buttonA1.getText().toString() == "O") {
            if (buttonA3.getText().toString() == "") {
                currentButton = buttonA3;
            }
            if (buttonC3.getText().toString() == "") {
                currentButton = buttonC3;
            }
            if (buttonC1.getText().toString() == "") {
                currentButton = buttonC1;
            }
        }

        if (buttonA3.getText().toString() == "O") {
            if (buttonA1.getText().toString() == "") {
                currentButton = buttonA1;
            }
            if (buttonC3.getText().toString() == "") {
                currentButton = buttonC3;
            }
            if (buttonC1.getText().toString() == "") {
                currentButton = buttonC1;
            }
        }

        if (buttonC3.getText().toString() == "O") {
            if (buttonA1.getText().toString() == "") {
                currentButton = buttonA1;
            }
            if (buttonA3.getText().toString() == "") {
                currentButton = buttonA3;
            }
            if (buttonC1.getText().toString() == "") {
                currentButton = buttonC1;
            }
        }

        if (buttonC1.getText().toString() == "O") {
            if (buttonA1.getText().toString() == "") {
                currentButton = buttonA1;
            }
            if (buttonA3.getText().toString() == "") {
                currentButton = buttonA3;
            }
            if (buttonC3.getText().toString() == "") {
                currentButton = buttonC3;
            }
        }
        return currentButton;
    }

    private Button EspaceVide() {
        Button button;
        for (Button currentButton : buttons) {
            if (currentButton != null) {
                if (currentButton.getText().toString() == "") {
                    button = currentButton;
                    return button;
                }
            }
        }
        return null;
    }
}
