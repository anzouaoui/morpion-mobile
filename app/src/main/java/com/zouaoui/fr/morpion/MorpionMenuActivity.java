package com.zouaoui.fr.morpion;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by zouaoui on 02/01/2017.
 */

public class MorpionMenuActivity extends AppCompatActivity {
    Button morpion2Joueurs;
    Button morpionOrdinateur;
    ImageView imageViewMoprion;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.morpion_menu_main);
        initialize();
        BindEventListener();
    }

    private void initialize() {
        morpion2Joueurs = (Button) findViewById(R.id.button2Joueurs);
        morpionOrdinateur = (Button) findViewById(R.id.buttonOrdinateur);
        imageViewMoprion = (ImageView) findViewById(R.id.imageViewMorpion);
    }

    private void BindEventListener() {
        morpion2Joueurs.setOnClickListener(morpion2JoueursListener);
        morpionOrdinateur.setOnClickListener(morpionOrdinateurListener);
    }

    private View.OnClickListener morpion2JoueursListener = new View.OnClickListener() {
        /**
         * Passer au jeux du morpion
         *
         * @param v
         */
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(MorpionMenuActivity.this, PlayersActivity.class);
            startActivity(appel);

        }
    };
    private View.OnClickListener morpionOrdinateurListener = new View.OnClickListener() {
        /**
         * Passer au jeux du morpion
         *
         * @param v
         */
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(MorpionMenuActivity.this, MorpionOrdinateurActivity.class);
            startActivity(appel);

        }
    };
}
