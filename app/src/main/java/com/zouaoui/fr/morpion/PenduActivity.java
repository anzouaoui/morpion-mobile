package com.zouaoui.fr.morpion;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.io.EOFException;


/**
 * Jeu du PENDU
 * Created by zouaoui on 06/01/2017.
 */

public class PenduActivity extends AppCompatActivity {
    LinearLayout container;
    Button send;
    TextView letters;
    ImageView pendu;
    EditText letter;

    private String wordToFind;
    private int wordFound;
    private int error;
    private List<Character> listOfLetters = new ArrayList<>();
    private boolean win;
    private List<String> wordList = new ArrayList<>();

    /**
     * Listener du bouton SEND
     */
    private View.OnClickListener sendListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            String letterInput = letter.getText().toString().toUpperCase();
            letter.setText("");

            if (letterInput.length() > 0){
                if(!letterUsed(letterInput.charAt(0), listOfLetters)){
                    listOfLetters.add(letterInput.charAt(0));
                    letterInWord(letterInput, wordToFind);
                }

                if(wordFound == wordToFind.length()){
                    win = true;
                    creeateDialog(win);
                }

                if(!wordToFind.contains(letterInput)){
                    error++;
                }
                setImage(error);
                if(error == 7){
                    win = false;
                    creeateDialog(win);
                }
                allLetters(listOfLetters);
            }
        }
    };

    /**
     * Listener de la boite de dialogue
     */
    private DialogInterface.OnClickListener alertDialogListener = new DialogInterface.OnClickListener(){
        @Override
        public void onClick(DialogInterface dialog, int which){
            newGame();
        }
    };

    /**
     * Affichage du layout
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pendu_main);

        initialize();
        newGame();
        BindEventListener();
    }

    /**
     * Initialisation des variables à partir des composants du layout
     */
    private void initialize() {
        container = (LinearLayout) findViewById(R.id.LinearLayoutWordContainer);
        send = (Button) findViewById(R.id.ButtonValid);
        letters = (TextView) findViewById(R.id.TextViewsletters);
        pendu = (ImageView) findViewById(R.id.ImageViewPendu);
        letter = (EditText) findViewById(R.id.EditTextLetter);
    }

    /**
     * Ecouter d'évenement
     */
    private void BindEventListener() {
        send.setOnClickListener(sendListener);
    }

    /**
     * Initialisation du jeu
     */
    public void newGame(){
        wordToFind = rdmWord();
        win = false;
        error = 0;
        wordFound = 0;
        letters.setText("");
        listOfLetters = new ArrayList<>();
        pendu.setBackgroundResource(R.drawable.zero);
        container.removeAllViews();
        for(int i=0 ; i< wordToFind.length() ; i++){
            TextView letter = (TextView) getLayoutInflater().inflate(R.layout.letter, null);
            container.addView(letter);
        }
    }

    /**
     * Verifier si la lettre a déja été saisie
     * @param currenLetter
     * @param listOfLetters
     * @return
     */
    public  boolean letterUsed(char currenLetter, List<Character> listOfLetters){
        boolean used = false;
        for(int j=0 ; j<listOfLetters.size(); j++){
            if(listOfLetters.get(j) == currenLetter)
            {
                Toast.makeText(getApplicationContext(), "Déjà saisie!!", Toast.LENGTH_SHORT).show();
                used = true;
            }
        }
        return used;
    }

    /**
     * Vérifier si la lettre est dans le mot
     * @param letter
     * @param word
     */
    public void letterInWord(String letter, String word){
        for(int k=0 ; k<word.length() ; k++ ){
            if(letter.equals(String.valueOf(word.charAt(k)))){
                TextView wordLetterFound = (TextView) container.getChildAt(k);
                wordLetterFound.setText((String.valueOf(word.charAt(k))));
                wordFound++;
            }
        }
    }

    /**
     * Récupérer toutes les lettres saisies
     * @param listOfLetters
     */
    public void allLetters(List<Character> listOfLetters){
        String collectionLetters = "";

        for (int l=0 ; l<listOfLetters.size(); l++){
            collectionLetters +=  listOfLetters.get(l)+"\n";
        }

        if(!collectionLetters.equals("")){
            letters.setText(collectionLetters);
        }
    }

    /**
     * Afficher les images en cas d'erreurs
     * @param error
     */
    public void setImage(int error){
        switch (error){
            case 1:
                pendu.setBackgroundResource(R.drawable.first);
                break;
            case 2:
                pendu.setBackgroundResource(R.drawable.second);
                break;
            case 3:
                pendu.setBackgroundResource(R.drawable.third);
                break;
            case 4:
                pendu.setBackgroundResource(R.drawable.fourth);
                break;
            case 5:
                pendu.setBackgroundResource(R.drawable.fifth);
                break;
            case 6:
                pendu.setBackgroundResource(R.drawable.sixth);
                break;
            case 7:
                pendu.setBackgroundResource(R.drawable.seventh);
                break;
        }
    }

    /**
     * Affichage d'une boite de dialogue indiquant si on a gagné ou perdu
     * @param win
     */
    public void creeateDialog(boolean win){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        if(win){
            alertDialog.setTitle("VOUS AVEZ GAGNE!!!");
        }
        else {
            alertDialog.setTitle("VOUS AVEZ PERDU!!!");
            alertDialog.setMessage("Le mot à trouver était: "+wordToFind);
        }
        alertDialog.setPositiveButton(getResources().getString(R.string.rejouer),alertDialogListener);
        alertDialog.create().show();
    }

    /**
     *
     * @return
     */
    public List<String> listWord(){
        try{
            BufferedReader buffer = new BufferedReader(new InputStreamReader(getAssets().open("dico.txt")));
            String word;
            while ((word = buffer.readLine()) != null){
                wordList.add(word);
            }
            buffer.close();
        }catch (IOException e){
            e.printStackTrace();
        }
        return wordList;
    }

    public String rdmWord(){
        wordList = listWord();
        int rdm = (int) (Math.floor(Math.random()*wordList.size()));
        String word = wordList.get(rdm).trim();
        return word;
    }
}
