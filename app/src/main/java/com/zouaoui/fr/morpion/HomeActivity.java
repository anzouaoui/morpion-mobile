package com.zouaoui.fr.morpion;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by zouaoui on 02/01/2017.
 */

public class HomeActivity extends AppCompatActivity {
    Button morpion;
    Button pendu;
    ImageView imageViewHome;
    Toolbar toolbar;
    private View.OnClickListener morpionListener = new View.OnClickListener() {
        /**
         * Passer au jeux du morpion
         *
         * @param v
         */
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(HomeActivity.this, MorpionMenuActivity.class);
            startActivity(appel);
        }
    };
    private View.OnClickListener penduListener = new View.OnClickListener() {
        /**
         * Passer au jeux du morpion
         *
         * @param v
         */
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(HomeActivity.this, PenduActivity.class);
            startActivity(appel);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_main);
        initialize();
        BindEventListener();
        setSupportActionBar(toolbar);
    }

    private void initialize() {
        morpion = (Button) findViewById(R.id.buttonMorpion);
        pendu = (Button) findViewById(R.id.buttonPendu);
        imageViewHome = (ImageView) findViewById(R.id.jeux);
    }

    private void BindEventListener() {
        morpion.setOnClickListener(morpionListener);
        pendu.setOnClickListener(penduListener);
    }
}
