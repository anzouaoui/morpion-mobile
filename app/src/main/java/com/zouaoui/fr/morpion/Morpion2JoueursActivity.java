package com.zouaoui.fr.morpion;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class Morpion2JoueursActivity extends AppCompatActivity {
    String player1 = "Premier_joueur";
    String player2 = "Second_joueur";

    String winner;
    String thePlayer;
    String firstPlayer;
    String secondPlayer;
    Button buttonA1, buttonA2, buttonA3, buttonB1, buttonB2, buttonB3, buttonC1, buttonC2, buttonC3;
    Button[] buttons = new Button[9];
    MediaPlayer music;
    TextView gameStatus;
    Button buttonReplay;
    ImageButton buttonPlay;
    ImageButton buttonStop;
    ImageView imageViewMorpion;
    Toolbar toolbar;
    boolean gameIsOver = false;
    int tour = 0;
    String[] symbol = {"X", "O"};

    /**
     * Initialisation de l'application
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.morpion_2_joueurs);

        Intent intent = getIntent();
         if (intent != null) {
             firstPlayer = intent.getStringExtra(player1);
             secondPlayer = intent.getStringExtra(player2);
         }
        initialize();
        BindEventListener();
    }

    /**
     * Gestion des évenements
     */
    private void BindEventListener() {
        for (Button button : buttons) {
            button.setOnClickListener(buttonListener);
        }
        buttonReplay.setOnClickListener(replayListener);

        buttonPlay.setOnClickListener(playListener);
        buttonStop.setOnClickListener(stopListener);
    }

    private void initialize() {
        buttonA1 = (Button) findViewById(R.id.buttonA1);
        buttonA2 = (Button) findViewById(R.id.buttonA2);
        buttonA3 = (Button) findViewById(R.id.buttonA3);
        buttonB1 = (Button) findViewById(R.id.buttonB1);
        buttonB2 = (Button) findViewById(R.id.buttonB2);
        buttonB3 = (Button) findViewById(R.id.buttonB3);
        buttonC1 = (Button) findViewById(R.id.buttonC1);
        buttonC2 = (Button) findViewById(R.id.buttonC2);
        buttonC3 = (Button) findViewById(R.id.buttonC3);
        buttonPlay = (ImageButton) findViewById(R.id.imageButtonPlay);
        buttonStop = (ImageButton) findViewById(R.id.imageButtonStop);
        gameStatus = (TextView) findViewById(R.id.gameStatusTextView);
        buttonReplay = (Button) findViewById(R.id.buttonReplay);
        imageViewMorpion = (ImageView) findViewById(R.id.morpion);
        music = MediaPlayer.create(Morpion2JoueursActivity.this, R.raw.bach);
        music.start();

        buttons[0] = buttonA1;
        buttons[1] = buttonA2;
        buttons[2] = buttonA3;
        buttons[3] = buttonB1;
        buttons[4] = buttonB2;
        buttons[5] = buttonB3;
        buttons[6] = buttonC1;
        buttons[7] = buttonC2;
        buttons[8] = buttonC3;

    }

    /**
     * Verifier que l'emplacement est valide
     *
     * @param button
     * @return boolean button
     */
    protected boolean isValid(Button button) {
        return button.getText().toString().length() == 0;
    }

    /**
     * Changer le texte du bouton selectionné
     *
     * @param button
     * @param symbol
     */
    protected void setSymbol(Button button, String symbol) {
        button.setText(symbol);
        if (button.getText().toString() == "X") {
            button.setTextColor(Color.rgb(0, 166, 166));
        } else if (button.getText().toString() == "O") {
            button.setTextColor(Color.rgb(138, 87, 17));
        }
    }

    protected boolean boardIsFull() {
        for (Button currentButton : buttons) {
            if (isValid(currentButton)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Lorsque q'un joueur gagne
     *
     * @return
     */
    protected boolean winnerExists() {
        /**
         * HORIZONTAL
         */
        if ((buttons[0].getText().toString() == symbol[tour]) && (buttons[1].getText().toString() == symbol[tour]) && (buttons[2].getText().toString() == symbol[tour])) {
            return true;
        }
        if ((buttons[3].getText().toString() == symbol[tour]) && (buttons[4].getText().toString() == symbol[tour]) && (buttons[5].getText().toString() == symbol[tour])) {
            return true;
        }
        if ((buttons[6].getText().toString() == symbol[tour]) && (buttons[7].getText().toString() == symbol[tour]) && (buttons[8].getText().toString() == symbol[tour])) {
            return true;
        }

        /**
         * VERTICAL
         */
        if ((buttons[0].getText().toString() == symbol[tour]) && (buttons[3].getText().toString() == symbol[tour]) && (buttons[6].getText().toString() == symbol[tour])) {
            return true;
        }
        if ((buttons[1].getText().toString() == symbol[tour]) && (buttons[4].getText().toString() == symbol[tour]) && (buttons[7].getText().toString() == symbol[tour])) {
            return true;
        }
        if ((buttons[2].getText().toString() == symbol[tour]) && (buttons[5].getText().toString() == symbol[tour]) && (buttons[8].getText().toString() == symbol[tour])) {
            return true;
        }

        /**
         * DIAGONAL
         */
        if ((buttons[0].getText().toString() == symbol[tour]) && (buttons[4].getText().toString() == symbol[tour]) && (buttons[8].getText().toString() == symbol[tour])) {
            return true;
        }
        if ((buttons[2].getText().toString() == symbol[tour]) && (buttons[4].getText().toString() == symbol[tour]) && (buttons[6].getText().toString() == symbol[tour])) {
            return true;
        }
        return false;
    }

    /**
     * Listener
     */
    private View.OnClickListener buttonListener = new View.OnClickListener() {
        /**
         * Cliquer sur les bouton
         * @param v
         */
        @Override
        public void onClick(View v) {
            //Trouver le bouton sur lequel on clique
            Button buttonSelected = (Button) findViewById(v.getId());

            //Verifier si le jeu est terminé
            if (gameIsOver) {
                return;
            }

            //Verifier si l'emplacement est valide
            if (!isValid(buttonSelected)) {
                //Mettre un animation

                //Afficher que le placement est invalide
                gameStatus.setText("Placement invalide");
            } else {
                setSymbol(buttonSelected, symbol[tour]);

                //Verfifier si il y a un gagnant
                gameIsOver = winnerExists();
                if (gameIsOver) {
                    if (symbol[tour] == "X") {
                        winner = firstPlayer;
                    }
                    else {
                        winner = secondPlayer;
                    }
                    gameStatus.setText(winner + " a gagné");
                    return;
                }

                //Si le tableau est plein
                if (boardIsFull()) {
                    gameStatus.setText("Match nul !!");
                    gameIsOver = true;
                    return;
                }
                if (tour == 0) {
                    tour = 1;
                    thePlayer = secondPlayer;
                } else {
                    tour = 0;
                    thePlayer = firstPlayer;
                }
                gameStatus.setText(thePlayer + " c'est à votre tour");
            }

        }
    };
    private View.OnClickListener replayListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            for (Button currentButton : buttons) {
                currentButton.setText("");
            }
            gameStatus.setText("A "+ firstPlayer + " de jouer");
            tour = 0;
            gameIsOver = false;
        }
    };
    private View.OnClickListener playListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            music = MediaPlayer.create(Morpion2JoueursActivity.this, R.raw.bach);
            music.start();
        }
    };
    private View.OnClickListener stopListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            music.stop();
        }
    };

}
