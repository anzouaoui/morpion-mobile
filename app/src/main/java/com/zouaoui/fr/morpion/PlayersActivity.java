package com.zouaoui.fr.morpion;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by zouaoui on 29/01/2017.
 */

public class PlayersActivity extends AppCompatActivity {

    String player1 = "Premier_joueur";
    String player2 = "Second_joueur";
    TextView titlePlayer;
    TextView firstPlayer;
    TextView secondPlayer;
    EditText first;
    EditText second;
    Button buttonPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.players);
        initialize();
        BindEventListener();
    }

    private void initialize() {
        titlePlayer = (TextView) findViewById(R.id.TextViewTitlePlayers);
        firstPlayer = (TextView) findViewById(R.id.TextViewFirstPlayer);
        secondPlayer = (TextView) findViewById(R.id.TextViewSecondPlayer);
        first = (EditText) findViewById(R.id.EditViewFirstPlayer);
        second = (EditText) findViewById(R.id.EditViewSecondPlayer);
        buttonPlayer = (Button) findViewById(R.id.ButtonPlayers);
    }

    private void BindEventListener() {
        buttonPlayer.setOnClickListener(buttonPlayerListener);
    }

    private View.OnClickListener buttonPlayerListener = new View.OnClickListener() {
        /**
         * Passer au jeux du morpion
         *
         * @param v
         */
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(PlayersActivity.this, Morpion2JoueursActivity.class);
            appel.putExtra(player1, first.getText().toString());
            appel.putExtra(player2, second.getText().toString());
            startActivity(appel);

        }
    };
}
